import Layout from "components/Layout";

function About() {
    return (
        <>
            <Layout>
                <h1>This is the about page!</h1>
            </Layout>
        </>
    )
}

export default About;